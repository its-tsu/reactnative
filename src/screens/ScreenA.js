import React, {Component} from 'react';
import {Text} from 'react-native';
import MainContainer from '../components/MainContainer';

export default class ScreenA extends Component {
  render() {
    const {navigate} = this.props.navigation;
    return (
      <MainContainer navigate={navigate}>
      <Text>Screen A</Text>
      </MainContainer>
    );
  }
}
